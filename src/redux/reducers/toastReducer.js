import { SET_TOAST } from "../actions/toastActionType";

const initialState = {
  snackbarOpen: false,
  snackbarType: "success",
  snackbarMessage: "",
};

export default function toastReducer(state = initialState, action) {
  switch (action.type) {
    case SET_TOAST:
      const { snackbarOpen, snackbarMessage, snackbarType } = action;
      return {
        ...state,
        snackbarOpen,
        snackbarType,
        snackbarMessage,
      };
    default:
      return state;
  }
}
import { combineReducers } from "redux";
import authReducer from "./authReducer";
import jobReducer from "./jobReducer";
import toastReducer from "./toastReducer";
import resetPasswordReducer from "./resetPasswordReducer";

const appReducer = combineReducers({
  authReducer,
  jobReducer,
  toastReducer,
  resetPasswordReducer,
});

export default function rootReducer(state, action) {
  return appReducer(state, action);
}
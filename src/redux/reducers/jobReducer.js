import {
  GET_POSTED_JOB,
  GET_ALL_POSTED_JOBS,
  SET_SHOW_POST_JOB,
  SELECT_POSTED_JOB,
  GET_ALL_CANDIDATES,
  GET_AVAILABLE_JOBS,
  GET_APPLIED_JOBS,
  SET_SHOW_APPLIED_JOBS,
} from "../actions/jobActionType";

const initialState = {
  showPostJob: false,
  showAppliedJobs: false,
  postedJobs: [],
  jobId: null,
  jobDetails: {},
  candidates: [],
  availableJobs: [],
  appliedJobs: [],
  count: 0,
};

export default function jobReducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_SHOW_POST_JOB:
      return { ...state, showPostJob: payload };
    case GET_ALL_POSTED_JOBS:
      return {
        ...state,
        postedJobs: payload.data,
        count: payload.metadata.count,
      };
    case SELECT_POSTED_JOB:
      return { ...state, jobId: payload };
    case GET_POSTED_JOB:
      return { ...state, jobDetails: payload.data };
    case GET_ALL_CANDIDATES:
      if (payload === []) {
        return { ...state, candidates: payload };
      } else {
        return { ...state, candidates: payload.data };
      }
    case GET_AVAILABLE_JOBS:
      return { ...state, availableJobs: payload };
    case GET_APPLIED_JOBS:
      return { ...state, appliedJobs: payload };
    case SET_SHOW_APPLIED_JOBS:
      return { ...state, showAppliedJobs: payload };
    default:
      return state;
  }
}

import {
  GET_POSTED_JOB,
  GET_ALL_POSTED_JOBS,
  SET_SHOW_POST_JOB,
  SELECT_POSTED_JOB,
  GET_ALL_CANDIDATES,
} from "./jobActionType";
import axios from "axios";
import { setToast } from "./toastAction";

const BASE_URL = process.env.REACT_APP_BASE_URL;

const apiDispatch = (actionType = "", data) => {
  return {
    type: actionType,
    payload: data,
  };
};

export const createJob = (jobData, history) => {
  const apiUrl = `${BASE_URL}/jobs`;
  return (dispatch) => {
    axios
      .post(apiUrl, jobData)
      .then((response) => {
        dispatch(setToast(true, "success", "Job posted successfully"));
        history.go(0);
      })
      .catch((error) => {
        dispatch(setToast(true, "error", "Error in job posting"));
      });
  };
};

export const getJobsList = (pageNo) => {
  const apiUrl = `${BASE_URL}/recruiters/jobs?page=${pageNo}`;
  return (dispatch) => {
    axios
      .get(apiUrl)
      .then((response) => {
        dispatch(apiDispatch(GET_ALL_POSTED_JOBS, response.data.data));
      })
      .catch((error) => {
        // dispatch(setToast(true, "info", "Create a Job"));
      });
  };
};

export const getJob = (id) => {
  const apiUrl = `${BASE_URL}/jobs/${id}`;
  return (dispatch) => {
    axios
      .get(apiUrl)
      .then((response) => {
        dispatch(apiDispatch(GET_POSTED_JOB, response.data));
      })
      .catch((error) => {
        dispatch(setToast(true, "error", "Error in selected Job"));
      });
  };
};

export const getAllCandidates = (id) => {
  const apiUrl = `${BASE_URL}/recruiters/jobs/${id}/candidates`;
  return (dispatch) => {
    axios
      .get(apiUrl)
      .then((response) => {
        if (response.data.message) {
          dispatch(apiDispatch(GET_ALL_CANDIDATES, []));
        } else {
          dispatch(apiDispatch(GET_ALL_CANDIDATES, response.data));
        }
      })
      .catch((error) => {
        dispatch(setToast(true, "error", "Error in getting candidates"));
      });
  };
};

export const setShowPostJob = (data) => {
  return (dispatch) => {
    dispatch({ type: SET_SHOW_POST_JOB, payload: data });
  };
};

export const setJobIdSelected = (id) => {
  return (dispatch) => {
    dispatch({ type: SELECT_POSTED_JOB, payload: id });
  };
};

import {
  GET_AVAILABLE_JOBS,
  GET_APPLIED_JOBS,
  SET_SHOW_APPLIED_JOBS,
} from "./jobActionType";

import axios from "axios";
import { setToast } from "./toastAction";

const BASE_URL = process.env.REACT_APP_BASE_URL;

const apiDispatch = (actionType = "", data) => {
  return {
    type: actionType,
    payload: data,
  };
};

export const applyJob = (jobData) => {
  const apiUrl = `${BASE_URL}/candidates/jobs`;

  return (dispatch) => {
    axios
      .post(apiUrl, jobData)
      .then((response) => {
        dispatch(setToast(true, "success", "Application sent successfully"));
        dispatch(getAppliedJobs());
      })
      .catch((error) => {
        if (error.response.status === 403) {
          dispatch(setToast(true, "info", "Already applied to this job"));
        } else {
          dispatch(setToast(true, "error", "Error in applying job"));
        }
      });
  };
};

export const getAvailableJobs = (pageNo) => {
  const apiUrl = `${BASE_URL}/candidates/jobs?page=${pageNo}`;
  return (dispatch) => {
    axios
      .get(apiUrl)
      .then((response) => {
        dispatch(apiDispatch(GET_AVAILABLE_JOBS, response.data.data));
      })
      .catch((error) => {
        dispatch(setToast(true, "error", "Error in getting Jobs"));
      });
  };
};

export const getAppliedJobs = (pageNo) => {
  const apiUrl = `${BASE_URL}/candidates/jobs/applied?page=${pageNo}`;

  return (dispatch) => {
    axios
      .get(apiUrl)
      .then((response) => {
        dispatch(apiDispatch(GET_APPLIED_JOBS, response.data.data));
      })
      .catch((error) => {
        dispatch(setToast(true, "info", "Error in getting Jobs"));
      });
  };
};

export const setShowAppliedJobs = (data) => {
  return (dispatch) => {
    dispatch({ type: SET_SHOW_APPLIED_JOBS, payload: data });
  };
};

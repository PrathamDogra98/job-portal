import { SET_TOAST } from "./toastActionType";

export const setToast = (
  snackbarOpen,
  snackbarType = "success",
  snackbarMessage = ""
) => ({
  type: SET_TOAST,
  snackbarOpen,
  snackbarType,
  snackbarMessage,
});
import axios from "axios";
import setAuthToken from "../../utils/setAuthToken";
import jwt_decode from "jwt-decode";
import { SET_CURRENT_USER, USER_LOADING } from "./authActionType";
import { setToast } from "./toastAction";

const BASE_URL = process.env.REACT_APP_BASE_URL;

// Register User
export const registerUser = (userData, history) => (dispatch) => {
  const apiUrl = `${BASE_URL}/auth/register`;

  axios
    .post(apiUrl, userData)
    .then((res) => {
      history.push("/");
      dispatch(setToast(true, "success", "User Registered Successfully"));
    })
    .catch((err) => {
      if (err.response.status === 500) {
        dispatch(setToast(true, "error", "Skillls are required for candidate"));
      } else if (err.response.status === 422) {
        if (err.response.data.errors) {
          dispatch(
            setToast(
              true,
              "error",
              "Skills and Name must be between 3 and 100 characters"
            )
          );
        } else {
          dispatch(setToast(true, "error", err.response.data.message));
        }
      } else {
        dispatch(setToast(true, "error", err.response.data.message));
      }
    });
};

// Login - get user token
export const loginUser = (userData, history) => (dispatch) => {
  const apiUrl = `${BASE_URL}/auth/login`;

  axios
    .post(apiUrl, userData)
    .then((response) => {
      const res = response.data;
      const { token } = res.data;
      localStorage.setItem("jwtToken", token);

      setAuthToken(token);

      const decoded = jwt_decode(token);
      if (decoded.userRole === 0) {
        history.push("/dashboard-recruiter");
      } else if (decoded.userRole === 1) {
        history.push("/dashboard-candidate");
      }

      dispatch(setCurrentUser(decoded));
    })
    .catch((error) => {
      if (error.response.status === 401) {
        dispatch(setToast(true, "error", error.response.data.message));
      }
    });
};

// Set logged in user
export const setCurrentUser = (data) => {
  return {
    type: SET_CURRENT_USER,
    payload: data,
  };
};

// User loading
export const setUserLoading = () => {
  return {
    type: USER_LOADING,
  };
};

// Log user out
export const logoutUser = () => (dispatch) => {
  localStorage.removeItem("jwtToken");

  setAuthToken(false);

  dispatch(setCurrentUser({}));
  dispatch(setToast(true, "success", "You have successfully logged out."));
};

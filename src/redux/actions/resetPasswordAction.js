import {
    GET_RESET_PASSWORD_TOKEN,
    CHANGE_PASSWORD,
    CHANGE_PASSWORD_ERROR,
    INVALID_EMAIL,
  } from "./resetPasswordActionType";
  import { setToast } from "./toastAction";
  import axios from "axios";
  
  const BASE_URL = process.env.REACT_APP_BASE_URL; 
  
  const apiDispatch = (actionType = "", data) => {
    return {
      type: actionType,
      payload: data,
    };
  };
  
  export const getResetPasswordToken = (email, history) => {
    const apiUrl = `${BASE_URL}/auth/resetpassword?email=${email}`;
    return (dispatch) => {
      axios
        .get(apiUrl)
        .then((response) => {
          dispatch(apiDispatch(GET_RESET_PASSWORD_TOKEN, response.data));
          history.push("/reset-password");
        })
        .catch((error) => {
          if (error.response.status === 404) {
            dispatch({ type: INVALID_EMAIL, payload: true });
            dispatch(setToast(true, "error", error.response.data.message));
          }
        });
    };
  };
  
  export const changePassword = (body, history) => {
    const apiUrl = `${BASE_URL}/auth/resetpassword`;
  
    return (dispatch) => {
      axios
        .post(apiUrl, body)
        .then((response) => {
          dispatch(apiDispatch(CHANGE_PASSWORD, response.data));
          dispatch(setToast(true, "success", "Password Updated Successfully"));
          history.push("/login");
        })
        .catch((error) => {
          dispatch(setToast(true, "error", error.response.data.message));
          dispatch(apiDispatch(CHANGE_PASSWORD_ERROR, error.response));
        });
    };
  };
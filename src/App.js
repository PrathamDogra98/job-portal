import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import store from "./redux/store";
import { routes, privateRoutes } from "./routes";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./redux/actions/authAction";
import PrivateRoute from "./components/private-route/PrivateRoute";
import ToastMessage from "./components/common/ToastMessage";
import ErrorPage from "./components/common/ErrorPage";
import "./styles/main.scss";

if (localStorage.jwtToken) {
  const token = localStorage.jwtToken;
  setAuthToken(token);

  const decoded = jwt_decode(token);

  store.dispatch(setCurrentUser(decoded));

  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    store.dispatch(logoutUser());
    window.location.href = "/";
  }
}

function App() {
  return (
    <>
      <Provider store={store}>
        <ToastMessage />
        <Router>
          <Switch>
            {routes.map((route, index) => {
              return (
                <Route
                  key={index}
                  path={route.path}
                  exact={route.exact}
                  component={route.component}
                />
              );
            })}

            {privateRoutes.map((route, index) => {
              return (
                <PrivateRoute
                  key={index}
                  exact
                  path={route.path}
                  component={route.component}
                />
              );
            })}
            <Route component={ErrorPage} />
          </Switch>
        </Router>
      </Provider>
    </>
  );
}

export default App;

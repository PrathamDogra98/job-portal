import Homepage from "../components/homepage/Homepage";
import Login from "../components/login/Login";
import Register from "../components/register/Register";
import DashboardCandidate from "../components/dashboard/DashboardCandidate";
import DashboardRecruiter from "../components/dashboard/DashboardRecruiter";
import ForgetPassword from "../components/password/ForgetPassword";
import ResetPassword from "../components/password/ResetPassword";

export const routes = [
  { path: "/", exact: true, component: Homepage },
  { path: "/login", exact: true, component: Login },
  { path: "/register", exact: true, component: Register },
  { path: "/forget-password", exact: true, component: ForgetPassword },
  { path: "/reset-password", exact: true, component: ResetPassword },
];

export const privateRoutes = [
  { path: "/dashboard-candidate", exact: true, component: DashboardCandidate },
  { path: "/dashboard-recruiter", exact: true, component: DashboardRecruiter },
];

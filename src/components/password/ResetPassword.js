import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import * as yup from "yup";
import { Formik, Form } from "formik";
import { TextField } from "../common/TextField";

import { changePassword } from "../../redux/actions/resetPasswordAction";
import { setToast } from "../../redux/actions/toastAction";

const validate = yup.object().shape({
  password: yup.string().required("Password is required").min(6).nullable(),
  confirmPassword: yup
    .string()
    .oneOf([yup.ref("password"), null], "Passwords must match")
    .nullable(),
  token: yup.string().required(),
});

const ResetPasswordForm = () => {
  const resetToken = useSelector(
    (state) => state.resetPasswordReducer.resetToken
  );
  const initialObj = {
    password: null,
    confirmPassword: null,
    token: resetToken,
  };
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (resetToken === null || resetToken === undefined) {
      dispatch(setToast(true, "error", "Token Expired. Go to forget password and enter email again"));
    }
  }, [resetToken]);
  
  return (
    <Formik
      initialValues={initialObj}
      validationSchema={validate}
      onSubmit={(obj) => {
        dispatch(changePassword(obj, history));
      }}
    >
      {(formik) => (
        <Form>
          <TextField
            label="New password"
            name="password"
            type="password"
            placeholder="Enter your password"
          />
          <TextField
            label="Confirm new password"
            name="confirmPassword"
            type="password"
            placeholder="Enter your password"
          />
          <div className="bottom-container">
            <button className="primary-button" type="submit">
              Reset
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default function ResetPassword() {
  const history = useHistory();

  const isAuthenticated = useSelector(
    (state) => state.authReducer.isAuthenticated
  );

  const user = useSelector((state) => state.authReducer.user);

  useEffect(() => {
    document.title = "Reset Password";

    if (isAuthenticated) {
      if (user.userRole === 0) {
        history.push("/dashboard-recruiter");
      } else if (user.userRole === 1) {
        history.push("/dashboard-candidate");
      }
    }
  }, [isAuthenticated, user]);

  return (
    <div>
      <div className="auth-container">
        <div className="upper-side">
          <div className="head-homepage">
            <a href="/">
              <div className="title-homepage">
                <span id="my">My</span>
                <span id="jobs">Jobs</span>
              </div>
            </a>
            <button
              className="login-button"
              onClick={() => history.push("/login")}
            >{`Login/Signup`}</button>
          </div>
          <hr className="dash-line" />
          <div className="form">
            <div className="form-content">
              <div className="head">Reset Your Password</div>
              <div className="head-text">Enter your new password below.</div>
              <ResetPasswordForm />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

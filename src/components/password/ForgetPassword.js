import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import * as yup from "yup";
import { Formik, Form } from "formik";
import { TextField } from "../common/TextField";
import { getResetPasswordToken } from "../../redux/actions/resetPasswordAction";

const initialObj = {
  email: null,
};

const validate = yup.object().shape({
  email: yup.string().email().required("Invalid Email").nullable(),
});

const ForgetPasswordForm = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  return (
    <Formik
      initialValues={initialObj}
      validationSchema={validate}
      onSubmit={(obj) => {
        dispatch(getResetPasswordToken(obj.email, history));
      }}
    >
      {(formik) => (
        <Form>
          <TextField
            label="Email address"
            name="email"
            type="email"
            placeholder="Enter your email"
          />

          <div className="bottom-container">
            <button className="primary-button" type="submit">
              Submit
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default function ForgetPassword() {
  const history = useHistory();

  const isAuthenticated = useSelector(
    (state) => state.authReducer.isAuthenticated
  );

  const user = useSelector((state) => state.authReducer.user);

  useEffect(() => {
    document.title = "Forget Password";

    if (isAuthenticated) {
      if (user.userRole === 0) {
        history.push("/dashboard-recruiter");
      } else if (user.userRole === 1) {
        history.push("/dashboard-candidate");
      }
    }
  }, [isAuthenticated, user]);

  return (
    <div>
      <div className="auth-container">
        <div className="upper-side">
          <div className="head-homepage">
            <a href="/">
              <div className="title-homepage">
                <span id="my">My</span>
                <span id="jobs">Jobs</span>
              </div>
            </a>
            <button
              className="login-button"
              onClick={() => history.push("/login")}
            >{`Login/Signup`}</button>
          </div>
          <hr className="dash-line" />
          <div className="form">
            <div className="form-content">
              <div className="head">Forgot your password?</div>
              <div className="head-text">
                Enter the email associated with your account and we’ll send you
                instructions to reset your password.
              </div>
              <ForgetPasswordForm />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

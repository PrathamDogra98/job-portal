import React, { useState, useEffect, useRef } from "react";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { logoutUser } from "../../redux/actions/authAction";

function UserProfile() {
  const [showLogin, setShowLogin] = useState(false);
  const dispatch = useDispatch();
  const [name, setUserName] = useState("C");
  const userName = useSelector((state) => state.authReducer.user.name);
  const history = useHistory();
  const logoutRef = useRef();

  const handleLogout = (event) => {
    event.preventDefault();
    dispatch(logoutUser());
    history.push("/");
  };

  useEffect(() => {
    if (userName !== undefined) {
      setUserName(userName);
    }
    const handler = (event) => {
      if (!logoutRef.current.contains(event.target)) {
        setShowLogin(false);
      }
    };
    document.addEventListener("mousedown", handler);
    return () => {
      document.removeEventListener("mousedown", handler);
    };
  }, [userName]);

  return (
    <>
      <div className="profile-container" ref={logoutRef}>
        <div className="user-profile">{name[0].toUpperCase()}</div>
        <i
          className="fa fa-caret-down"
          onClick={() => {
            setShowLogin(!showLogin);
          }}
        ></i>
        <button
          className={`logout-btn ${showLogin === true ? "active" : ""}`}
          onClick={handleLogout}
        >{`Log out`}</button>
      </div>
    </>
  );
}

export default UserProfile;

import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import * as yup from "yup";
import { Formik, Form } from "formik";
import { TextField } from "../../common/TextField";
import {
  createJob,
  setShowPostJob,
} from "../../../redux/actions/recruiterJobAction";
import UserProfile from "../UserProfile";

const initialObj = {
  title: null,
  description: null,
  location: null,
};

const validate = yup.object().shape({
  title: yup.string().required("Required").nullable(),
  description: yup.string().required("Required").nullable(),
  location: yup.string().required("Required").nullable(),
});

const PostJobForm = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  return (
    <Formik
      initialValues={initialObj}
      validationSchema={validate}
      onSubmit={(obj) => {
        dispatch(createJob(obj, history));
      }}
    >
      {(formik) => (
        <Form>
          <TextField
            label="Job title*"
            name="title"
            type="text"
            placeholder="Enter job title"
          />
          <TextField
            label="Description*"
            name="description"
            type="textarea"
            placeholder="Enter job description"
          />
          <TextField
            label="Location*"
            name="location"
            type="text"
            placeholder="Enter location"
          />
          <div className="bottom-container">
            <button className="primary-button" type="submit">
              Post
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default function PostJob() {
  const history = useHistory();
  const dispatch = useDispatch();

  const isAuthenticated = useSelector(
    (state) => state.authReducer.isAuthenticated
  );
  const showPostJob = useSelector((state) => state.jobReducer.showPostJob);
  const user = useSelector((state) => state.authReducer.user);

  useEffect(() => {
    document.title = "Post Job";

    if (isAuthenticated) {
      if (user.userRole === 0) {
        history.push("/dashboard-recruiter");
      } else if (user.userRole === 1) {
        history.push("/dashboard-candidate");
      }
    }
  }, [isAuthenticated]);

  return (
    <div>
      <div className="auth-container">
        <div className="upper-side">
          <div className="head-homepage">
            <div className="title-homepage">
              <span id="my">My</span>
              <span id="jobs">Jobs</span>
            </div>

            <span
              className={`post-job ${showPostJob === true ? "active" : ""}`}
            >
              Post a Job
            </span>
            <UserProfile />
          </div>

          <hr className="dash-line" />
          <div className="tag-line-container">
            <div className="tag-line">
              {showPostJob ? (
                <div
                  onClick={() => {
                    dispatch(setShowPostJob(false));
                  }}
                  className="back-btn"
                >
                  Back
                </div>
              ) : null}
            </div>
          </div>
          <div className="form">
            <div className="form-content">
              <div className="head">Post a Job</div>
              <PostJobForm />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

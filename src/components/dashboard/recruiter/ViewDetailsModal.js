import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Col, Row, Modal } from "react-bootstrap";

function ViewDetailsModal({ jobId, show, onHide }) {
  const [applicants, setApplicants] = useState([]);
  const candidates = useSelector((state) => state.jobReducer.candidates);

  useEffect(() => {
    if (jobId) {
      setApplicants([]);
    }
    if (candidates !== undefined) {
      setApplicants(candidates);
    }
  }, [candidates, jobId]);
  return (
    <Modal show={show} centered onHide={onHide} scrollable={true}>
      <Modal.Body>
        <div className="modal-container">
          <div className="row heading-container">
            <div className="col">
              <div clssName="modal-heading">Applicants for this job</div>
            </div>
            <div className="col cross-container">
              <div className="cross-btn" onClick={onHide}>
                X
              </div>
            </div>
          </div>
          <hr className="modal-line"></hr>
          <div className="modal-text">{`Total ${
            applicants.length ? applicants.length : 0
          } applications`}</div>
          <div className="main-container">
            <ul className="cards">
              {applicants.length ? (
                applicants.map((applicant, key) => {
                  return (
                    <li className="cards_item" key={key}>
                      <div className="card">
                        <div className="card_content">
                          <div className="card-header">
                            <div className="profile-pic">{applicant.name[0].toUpperCase()}</div>
                            <div className="card_title">
                              <div className="name">{applicant.name}</div>
                              <p className="email">{applicant.email}</p>
                            </div>
                          </div>
                          <div className="skills-container">
                            <div className="skill-head">Skills</div>
                            <div className="skill-content">
                              {applicant.skills}
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                  );
                })
              ) : (
                <div className="no-applicants">
                  <div className="no-applicants-logo">
                    
                  </div>
                  <div className="no-content">
                      {"No applications available!"}
                    </div>
                </div>
              )}
            </ul>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
}

export default ViewDetailsModal;

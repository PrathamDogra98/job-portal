import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { Dialog, IconButton, Typography } from "@material-ui/core";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import CloseIcon from "@material-ui/icons/Close";

import { makeStyles } from "@material-ui/core/styles";
import { getAllCandidates } from "../../../redux/actions/recruiterJobAction";

const styles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
}));

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6" style={{ fontWeight: "bold" }}>
        {children}
      </Typography>
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const JobDetailsModal = ({ jobId, show, onHide }) => {
  const [applicants, setApplicants] = useState([]);
  const candidates = useSelector((state) => state.jobReducer.candidates);

  useEffect(() => {
    if (jobId) {
      setApplicants([]);
    }
    if (candidates !== undefined) {
      setApplicants(candidates);
    }
  }, [candidates, jobId]);

  return (
    <Dialog
      onClose={onHide}
      aria-labelledby="customized-dialog-title"
      open={show}
      fullWidth
    >
      <DialogTitle id="customized-dialog-title" onClose={onHide}>
        Applicants for this job
      </DialogTitle>
      <DialogContent dividers>
        <div className="total-applicants">{`Total ${
          applicants.length ? applicants.length : 0
        } applications`}</div>
        <div className="main-container">
          <ul className="cards">
            {applicants.length ? (
              applicants.map((applicant, key) => {
                return (
                  <li className="cards_item" key={key}>
                    <div className="card">
                      <div className="card_content">
                        <h2 className="card_title">{applicant.name}</h2>
                        <p className="card_text">{applicant.email}</p>
                        <div className="modal-bottom">
                          <div className="location">{`Skills: ${applicant.skills}`}</div>
                        </div>
                      </div>
                    </div>
                  </li>
                );
              })
            ) : (
              <div className="no-jobs-container">
                <div className="no-jobs">
                  <div className="no-jobs-1">
                    {"No applications available!"}
                  </div>
                </div>
              </div>
            )}
          </ul>
        </div>
      </DialogContent>
      <DialogActions></DialogActions>
    </Dialog>
  );
};

export default JobDetailsModal;

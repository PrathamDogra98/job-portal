import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import JobDetailsModal from "./JobDetailsModal";
import {
  getAllCandidates,
  setShowPostJob,
} from "../../../redux/actions/recruiterJobAction";
import ViewDetailsModal from "./ViewDetailsModal";
import Pagination from "../../common/Pagination";

function PostedJobsList() {
  const postedJobs = useSelector((state) => state.jobReducer.postedJobs);
  const [showDetailsModal, setShowDetailsModal] = useState(false);
  const [jobId, setJobId] = useState(null);

  const [jobArr, setJobArr] = useState(postedJobs);
  const dispatch = useDispatch();

  useEffect(() => {
    if (postedJobs !== undefined) {
      setJobArr(postedJobs);
    }
  }, [postedJobs]);

  const handleShowModal = (id) => () => {
    setJobId(id);
    dispatch(getAllCandidates(id));
    setShowDetailsModal(true);
  };

  const onCloseModal = () => {
    setShowDetailsModal(false);
  };

  return (
    <>
      {jobArr.length ? (
        <>
          <div className="main">
            <ul className="cards">
              {jobArr.map((job, key) => {
                return (
                  <li className="cards_item" key={key}>
                    <div className="card">
                      <div className="card_content">
                        <h2 className="card_title">{job.title}</h2>
                        <p className="card_text">{job.description}</p>
                        <div className="card-bottom">
                          <div className="card-icon">
                            <i
                              className="fa fa-map-marker"
                              aria-hidden="true"
                            ></i>
                          </div>
                          <div className="location-container">
                            <div className="location">{job.location}</div>
                          </div>
                          <div className="btn-container-view">
                            <button
                              className="btn-view"
                              onClick={handleShowModal(job.id)}
                            >
                              View applications
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                );
              })}
            </ul>
          </div>
          <Pagination />
        </>
      ) : (
        <div className="no-jobs-container">
          <div className="nothing-icon"></div>
          <div className="no-jobs">{"Your posted jobs will show here!"}</div>
          <button
            className="primary-button"
            onClick={() => dispatch(setShowPostJob(true))}
          >
            Post a Job
          </button>
        </div>
      )}
      <ViewDetailsModal
        show={showDetailsModal}
        onHide={onCloseModal}
        jobId={jobId}
      />
    </>
  );
}

export default PostedJobsList;

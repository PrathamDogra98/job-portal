import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import JobsList from "./candidate/JobsList";
import AppliedJobs from "./candidate/AppliedJobs";
import UserProfile from "./UserProfile";
import {
  getAvailableJobs,
  getAppliedJobs,
  setShowAppliedJobs,
} from "../../redux/actions/candidateJobAction";
import Pagination from "../common/Pagination";

function DashboardCandidate() {
  const history = useHistory();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.authReducer.user);
  const isAuthenticated = useSelector(
    (state) => state.authReducer.isAuthenticated
  );
  const showAppliedJobs = useSelector(
    (state) => state.jobReducer.showAppliedJobs
  );
  const jobList = useSelector((state) => state.jobReducer.availableJobs);

  const handleBack = () => {
    dispatch(setShowAppliedJobs(false));
  };
  useEffect(() => {
    if (isAuthenticated) {
      if (user.userRole === 0) {
        history.push("/dashboard-recruiter");
      } else if (user.userRole === 1) {
        history.push("/dashboard-candidate");
      }
      dispatch(getAvailableJobs(1));
      dispatch(getAppliedJobs(1));
      dispatch(setShowAppliedJobs(false));
    }
  }, [isAuthenticated, user]);

  return (
    <div className="dashboard-container">
      <div className="recruiter-nav">
        <div className="head-homepage">
          <div className="title-homepage">
            <span id="my">My</span>
            <span id="jobs">Jobs</span>
          </div>
          <span
            className={`apply-jobs ${showAppliedJobs === true ? "active" : ""}`}
            onClick={() => dispatch(setShowAppliedJobs(true))}
          >
            Applied Jobs
          </span>
          <UserProfile />
        </div>
        <hr className="dash-line" />
        <div className="tag-line-container">
          {showAppliedJobs ? (
            <div onClick={handleBack} className="back-btn padding-left">
              Back
            </div>
          ) : null}
          <div className="tag-line">
            {showAppliedJobs ? `Jobs applied by you` : `Jobs for you`}
          </div>
        </div>
      </div>
      <div className="posted-jobs-container">
        {showAppliedJobs ? <AppliedJobs /> : <JobsList />}
        {showAppliedJobs ? null : jobList.length ? <Pagination /> : null}
      </div>
    </div>
  );
}

export default DashboardCandidate;

import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import UserProfile from "./UserProfile";
import PostedJobsList from "./recruiter/PostedJobsList";
import {
  getJobsList,
  setShowPostJob,
} from "../../redux/actions/recruiterJobAction";
import Pagination from "../common/Pagination";
import PostJob from "./recruiter/PostJob";

function DashboardRecruiter() {
  const history = useHistory();
  const dispatch = useDispatch();

  const user = useSelector((state) => state.authReducer.user);
  const showPostJob = useSelector((state) => state.jobReducer.showPostJob);
  const isAuthenticated = useSelector(
    (state) => state.authReducer.isAuthenticated
  );


  const handlePostJob = () => {
    dispatch(setShowPostJob(true));
  };

  useEffect(() => {
    document.title = "Recruiter";

    if (isAuthenticated) {
      if (user.userRole === 0) {
        history.push("/dashboard-recruiter");
      } else if (user.userRole === 1) {
        history.push("/dashboard-candidate");
      }
      dispatch(getJobsList(1));
    }
  }, [isAuthenticated, user]);

  return (
    <>
      {showPostJob ? (
        <PostJob />
      ) : (
        <div className="dashboard-container">
          <div className="recruiter-nav">
            <div className="head-homepage">
              <div className="title-homepage">
                <span id="my">My</span>
                <span id="jobs">Jobs</span>
              </div>
              <span className="post-job" onClick={handlePostJob}>
                Post a Job
              </span>
              <UserProfile />
            </div>
            <hr className="dash-line" />
            <div className="tag-line-container">
              <div className="tag-line">{`Jobs posted by you`}</div>
            </div>
            <div className="posted-jobs-container">
              <PostedJobsList />
            </div>
          </div>
        </div>
      )}

    </>
  );
}

export default DashboardRecruiter;

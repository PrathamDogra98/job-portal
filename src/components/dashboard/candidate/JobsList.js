import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { applyJob } from "../../../redux/actions/candidateJobAction";

function JobsList() {
  const [jobArr, setJobArr] = useState([]);
  const dispatch = useDispatch();
  const jobList = useSelector((state) => state.jobReducer.availableJobs);

  useEffect(() => {
    document.title = "Candidate";
    if (jobList !== []) {
      setJobArr(jobList);
    }
  }, [jobList]);

  const handleApply = (id) => () => {
    dispatch(applyJob({ jobId: id }));
  };
  return (
    <>
      {jobArr.length ? (
        <div className="main">
          <ul className="cards">
            {jobArr.map((job, key) => {
              return (
                <li className="cards_item" key={key}>
                  <div className="card">
                    <div className="card_content">
                      <h2 className="card_title">{job.title}</h2>
                      <p className="card_text">{job.description}</p>
                      <div className="card-bottom">
                        <div className="card-icon">
                          <i
                            className="fa fa-map-marker"
                            aria-hidden="true"
                          ></i>
                        </div>
                        <div className="location-container">
                          <div className="location">{job.location}</div>
                        </div>
                        <div className="btn-container-apply">
                          <button
                            className="btn-apply"
                            onClick={handleApply(job.id)}
                          >
                            Apply
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      ) : (
        <div className="no-jobs-container">
          <div className="nothing-icon"></div>
          <div className="no-jobs">{"No available Jobs for you!"}</div>
        </div>
      )}
    </>
  );
}

export default JobsList;

import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { setShowAppliedJobs } from "../../../redux/actions/candidateJobAction";

function AppliedJobs() {
  const [jobArr, setJobArr] = useState([]);
  const dispatch = useDispatch();

  const appliedJobs = useSelector((state) => state.jobReducer.appliedJobs);

  useEffect(() => {
    document.title = "Applied Jobs";

    if (appliedJobs !== undefined) {
      setJobArr(appliedJobs);
    }
  }, [appliedJobs]);

  const handleBack = () => {
    dispatch(setShowAppliedJobs(false));
  };

  return (
    <>
      {jobArr.length ? (
        <div className="main">
          <ul className="cards">
            {jobArr.map((job, key) => {
              return (
                <li className="cards_item" key={key}>
                  <div className="card">
                    <div className="card_content">
                      <h2 className="card_title">{job.title}</h2>
                      <p className="card_text">{job.description}</p>
                      <div className="card-bottom">
                        <div className="card-icon">
                          <i
                            className="fa fa-map-marker"
                            aria-hidden="true"
                          ></i>
                        </div>
                        <div className="location-container">
                          <div className="location">{job.location}</div>
                        </div>
                        <div className="btn-container"></div>
                      </div>
                    </div>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      ) : (
        <div className="no-jobs-container">
          <div className="nothing-icon"></div>
          <div className="no-jobs">{"Your applied jobs will show here!"}</div>
          <button className="primary-button" onClick={handleBack}>
            See all jobs
          </button>
        </div>
      )}
    </>
  );
}

export default AppliedJobs;

import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import * as yup from "yup";
import RegisterForm from "./RegisterForm";
import { registerUser } from "../../redux/actions/authAction";

const useStyles = makeStyles((theme) => ({
  form: {
    width: "100%",
    marginTop: theme.spacing(1),
    "& .MuiTextField-root": {
      "& .MuiInputLabel-outlined.MuiInputLabel-shrink": {
        transform: "translate(14px,2px) scale(0.75)",
      },
    },
  },
  textFiled: {
    minHeight: "1rem",
  },
}));

function Register() {
  const [userRole, setUserRole] = useState(0);
  const [userObj, setUserObj] = useState({});
  const history = useHistory();
  const dispatch = useDispatch();

  const isAuthenticated = useSelector(
    (state) => state.authReducer.isAuthenticated
  );
  const user = useSelector((state) => state.authReducer.user);

  useEffect(() => {
    document.title = "Sign Up";

    if (isAuthenticated) {
      if (user.userRole === 0) {
        history.push("/dashboard-recruiter");
      } else if (user.userRole === 1) {
        history.push("/dashboard-candidate");
      }
    }
    setUserRole(userRole);
  }, [isAuthenticated, user, userRole, userObj]);

  const handleFormObj = (formObj) => {
    dispatch(
      registerUser(
        { ...formObj, userRole, name: formObj.name.replace(/ /g, "") },
        history
      )
    );
    setUserObj({ ...formObj, userRole, name: formObj.name.replace(/ /g, "") });
  };

  const handleUserType = (key) => () => {
    if (key === 0) {
      setUserRole(key);
    } else if (key === 1) {
      setUserRole(key);
    }
  };

  return (
    <div className="auth-container">
      <div className="upper-side">
        <div className="head-homepage">
          <a href="/">
            <div className="title-homepage">
              <span id="my">My</span>
              <span id="jobs">Jobs</span>
            </div>
          </a>
        </div>
        <hr className="dash-line" />
        <div className="form top-35">
          <div className="form-content">
            <div className="head">Signup</div>
            <div className="head-text"> {`I’m a*`}</div>
            <div className="user-type">
              <button
                className={`user-type-0 ${userRole === 0 ? "active" : ""}`}
                onClick={handleUserType(0)}
              >
                Recruiter
              </button>
              <button
                className={`user-type-1 ${userRole === 1 ? "active" : ""}`}
                onClick={handleUserType(1)}
              >
                Candidate
              </button>
            </div>
            <div className="role-error">
              {userRole !== null ? "" : "Select role"}
            </div>
            <RegisterForm handleFormObj={handleFormObj} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Register;

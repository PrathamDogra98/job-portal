import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Formik, Form } from "formik";
import { TextField } from "../common/TextField";
import * as yup from "yup";
import { registerUser } from "../../redux/actions/authAction";

const RegisterForm = ({ handleFormObj }) => {
  const initialUser = {
    email: null,
    userRole: null,
    password: null,
    confirmPassword: null,
    name: null,
    skills: null,
  };
  const isAuthenticated = useSelector(
    (state) => state.authReducer.isAuthenticated
  );
  const user = useSelector((state) => state.authReducer.user);
  // const dispatch = useDispatch();
  const history = useHistory();
  const validate = yup.object().shape({
    email: yup.string().email().required().nullable(),
    // userRole: yup.number().required().nullable(),
    password: yup.string().required("Password is required").min(6).nullable(),
    confirmPassword: yup
      .string()
      .oneOf([yup.ref("password"), null], "Passwords must match")
      .nullable(),

    name: yup.string().required("Required").min(3).nullable(),
    skills: yup.string().min(3).nullable(),
  });

  useEffect(() => {
    document.title = "Sign Up";

    if (isAuthenticated) {
      if (user.userRole === 0) {
        history.push("/dashboard-recruiter");
      } else if (user.userRole === 1) {
        history.push("/dashboard-candidate");
      }
    }
  }, [isAuthenticated]);

  const handleSubmit = (user) => {
    handleFormObj(user);
  };

  return (
    <Formik
      initialValues={initialUser}
      validationSchema={validate}
      onSubmit={(user) => {
        handleSubmit(user);
      }}
    >
      {(formik) => (
        <Form>
          <TextField
            label="Full Name*"
            name="name"
            type="text"
            placeholder="Full Name"
            // value={initialUser.name}
          />
          <TextField
            label="Email address*"
            name="email"
            type="email"
            placeholder="Enter your email"
            // value={initialUser.email}
          />
          <div className="password-container">
            <TextField
              label="Create Password*"
              name="password"
              type="password"
              placeholder="Enter your password"
              // value={initialUser.password}
            />
            <TextField
              label="Confirm Password*"
              name="confirmPassword"
              type="password"
              placeholder="Enter your password"
              // value={initialUser.confirmPassword}
            />
          </div>
          <TextField
            label="Skills"
            name="skills"
            placeholder="Enter your skills"
            // value={initialUser.skills}
          />
          <div className="bottom-container">
            <button className="primary-button" type="submit">
              Signup
            </button>
            <div className="login-text">
              Have an account? <a href="/login">Login</a>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default RegisterForm;

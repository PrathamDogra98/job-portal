import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import photos from "../../assets/photos.png";
import apple from "../../assets/apple.png";
import icloud from "../../assets/icloud.png";
import logo from "../../assets/logo.png";
import slack from "../../assets/slack.png";
import { setToast } from "../../redux/actions/toastAction";

function Homepage() {
  const history = useHistory();
  const dispatch = useDispatch();
  const isAuthenticated = useSelector(
    (state) => state.authReducer.isAuthenticated
  );
  const user = useSelector((state) => state.authReducer.user);

  useEffect(() => {
    document.title = "MyJobs";

    if (isAuthenticated) {
      if (user.userRole === 0) {
        history.push("/dashboard-recruiter");
      } else if (user.userRole === 1) {
        history.push("/dashboard-candidate");
      }
    }
  }, [isAuthenticated]);

  const handleStart = () => {
    dispatch(setToast(true, "info", "Login/SignUp to get started"));
  };

  return (
    <>
      <div className="container-homepage">
        <div className="upper-div">
          <div className="head-homepage">
            <div className="title-homepage">
              <span id="my">My</span>
              <span id="jobs">Jobs</span>
            </div>
            <button
              className="login-button"
              onClick={() => history.push("/login")}
            >{`Login/Signup`}</button>
          </div>
          <hr className="dash-line" />
          <div className="content-block">
            <div className="content">
              <div>Welcome to</div>
              <div className="myJobs">
                <span id="my">My</span>
                <span id="jobs">Jobs</span>
              </div>
              <button
                className="primary-button"
                onClick={handleStart}
              >{`Get Started`}</button>
            </div>
          </div>
          <div className="why-us">
            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="why-us-tagline">Why us</div>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <div className="why-us-card">
                    <div className="heading">Get more visibility</div>
                    <div className="text">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt.
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className="why-us-card">
                    <div className="heading">Organize your candidates</div>
                    <div className="text">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua.
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className="why-us-card">
                    <div className="heading">Verify their abilities</div>
                    <div className="text">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore.
                    </div>
                  </div>
                </div>
              </div>
              <div className="trust-us-container">
                <div className="row">
                  <div className="col">
                    <div className="trust-us-tagline">
                      companies who trust us
                    </div>
                  </div>
                </div>
                <div className="company-logos">
                  <div className="row">
                    <div className="col-md">
                      <img src={photos} alt="Logo" width="40px" height="40px" />
                    </div>
                    <div className="col-md ">
                      <img src={apple} alt="Logo" width="40px" height="40px" />{" "}
                    </div>
                    <div className="col-md">
                      <img src={icloud} alt="Logo" width="40px" height="40px" />{" "}
                    </div>
                    <div className="col-md">
                      <img src={logo} alt="Logo" width="40px" height="40px" />{" "}
                    </div>
                    <div className="col-md">
                      <img src={slack} alt="Logo" width="40px" height="40px" />{" "}
                    </div>
                  </div>
                  <div className="row padding-top">
                    <div className="col-md">
                      <img src={photos} alt="Logo" width="40px" height="40px" />
                    </div>
                    <div className="col-md ">
                      <img src={apple} alt="Logo" width="40px" height="40px" />{" "}
                    </div>
                    <div className="col-md">
                      <img src={icloud} alt="Logo" width="40px" height="40px" />{" "}
                    </div>
                    <div className="col-md">
                      <img src={logo} alt="Logo" width="40px" height="40px" />{" "}
                    </div>
                    <div className="col-md">
                      <img src={slack} alt="Logo" width="40px" height="40px" />{" "}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="picture"></div>
      </div>
    </>
  );
}

export default Homepage;

import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Formik, Form } from "formik";
import { TextField } from "../common/TextField";
import * as yup from "yup";
import { loginUser } from "../../redux/actions/authAction";

const LoginForm = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const initialUser = {
    email: null,
    password: null,
  };

  const validate = yup.object().shape({
    email: yup.string().email().required("Invalid Email").nullable(),
    password: yup.string().required("Invalid Password").min(6).nullable(),
  });

  return (
    <Formik
      initialValues={initialUser}
      validationSchema={validate}
      onSubmit={(user) => {
        dispatch(loginUser(user, history));
      }}
    >
      {(formik) => (
        <div className="form-content">
          <div className="head">Login</div>
          <Form>
            <TextField
              label="Email address*"
              name="email"
              type="email"
              placeholder="Enter your email"
            />
            <TextField
              label="Password*"
              name="password"
              type="password"
              placeholder="Enter your password"
            />
            <div className="bottom-container">
              <button className="primary-button" type="submit">
                Login
              </button>
              <div className="login-text">
                New to MyJobs? <a href="/register">Create an account</a>
              </div>
            </div>
          </Form>
        </div>
      )}
    </Formik>
  );
};

export default LoginForm;

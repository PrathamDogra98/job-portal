import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import LoginForm from "./LoginForm";

export default function Login() {
  const isAuthenticated = useSelector(
    (state) => state.authReducer.isAuthenticated
  );
  const user = useSelector((state) => state.authReducer.user);
  const history = useHistory();

  useEffect(() => {
    document.title = "Login";

    if (isAuthenticated) {
      if (user.userRole === 0) {
        history.push("/dashboard-recruiter");
      } else if (user.userRole === 1) {
        history.push("/dashboard-candidate");
      }
    }
  }, [isAuthenticated, user]);

  return (
    <div>
      <div className="auth-container">
        <div className="upper-side">
          <div className="head-homepage">
            <a href="/">
              <div className="title-homepage">
                <span id="my">My</span>
                <span id="jobs">Jobs</span>
              </div>
            </a>
          </div>
          <hr className="dash-line" />
          <div className="form">
            <LoginForm />
          </div>
        </div>
      </div>
    </div>
  );
}

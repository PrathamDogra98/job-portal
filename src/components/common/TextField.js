import React from "react";
import { ErrorMessage, useField } from "formik";

export const TextField = ({ label, ...props }) => {
  const [field, meta] = useField(props);

  return (
    <div className="text-field">
      <div className="label-container">
        <label htmlFor={field.name}>{label}</label>
        {label === "Password*" ? (
          <div className="label-text">
            <a href="/forget-password">Forgot your password?</a>
          </div>
        ) : (
          ""
        )}
      </div>

      <input
        className={`form-control shadow-none ${
          meta.touched && meta.error && "is-invalid"
        }`}
        {...field}
        {...props}
        autoComplete="off"
      />
      <ErrorMessage component="div" name={field.name} className="error" />
    </div>
  );
};

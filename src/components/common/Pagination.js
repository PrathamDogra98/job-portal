import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getJobsList } from "../../redux/actions/recruiterJobAction";
import {
  getAvailableJobs,
  getAppliedJobs,
} from "../../redux/actions/candidateJobAction";

function Pagination() {
  const [pageNo, setPageNo] = useState(1);
  const dispatch = useDispatch();
  const [pageCount, setPageCount] = useState(0);
  const count = useSelector((state) => state.jobReducer.count);
  const isAuthenticated = useSelector(
    (state) => state.authReducer.isAuthenticated
  );
  const user = useSelector((state) => state.authReducer.user);
  const showAppliedJobs = useSelector(
    (state) => state.jobReducer.showAppliedJobs
  );

  const handlePageNo = (num) => {
    setPageNo(num);
    if (isAuthenticated) {
      if (user.userRole === 0) {
        dispatch(getJobsList(num));
      } else if (user.userRole === 1) {
        if (showAppliedJobs) {
          setPageNo(num);
          dispatch(getAppliedJobs(num));
        } else {
          setPageNo(num);
          dispatch(getAvailableJobs(num));
        }
      }
    }
  };

  useEffect(() => {
    setPageCount(count);
  }, [count]);

  const pages = Math.ceil(pageCount / 20);
  //   let pagesArr = [];
  //   for (let i = 1; i <= pages; i++) {
  //     pagesArr.push(i);
  //   }

  return (
    <div className="container-pagination">
      <button
        disabled={pageNo - 1 === 0}
        onClick={() => handlePageNo(pageNo - 1)}
        className={`pagination-button ${pageNo - 1 === 0 ? "disabled" : null}`}
      >
        <i className="fa fa-caret-left" />
      </button>

      <div className="pagination-center">
        <div className="page">{pageNo}</div>
      </div>

      <button
        disabled={pageNo === pages}
        onClick={() => handlePageNo(pageNo + 1)}
        className={`pagination-button ${pageNo === pages ? "disabled" : null}`}
      >
        <i className="fa fa-caret-right" />
      </button>
    </div>
  );
}

export default Pagination;

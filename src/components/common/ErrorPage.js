import React from "react";
import pic from "../../assets/Astronaut-big.png";

function ErrorPage() {
  return (
    <div className="error-page">
      <div className="error-text"> 404 Page not found</div>
      <div>
        <img src={pic} alt="" width="100px" height="100px" />
      </div>
    </div>
  );
}

export default ErrorPage;
